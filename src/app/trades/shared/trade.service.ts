import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Trade } from './trade';

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  startedEditing = new Subject<number>();
  listChanged = new Subject<Trade[]>();
  deleted = new Subject<boolean>();

  Trades: Trade[] = [];
  Trade: Trade;
  constructor(private http: HttpClient) { }

  add(entity: Trade): Observable<Trade> {
    return this.http.post<Trade>("http://localhost:59100/api/" + "Trades", entity);
  }

  update(id: number, entity: Trade): Observable<any> {
    entity.id = id;
    return this.http.put("http://localhost:59100/api/" + "Trades/" + id, entity);
  }

  delete(id: number): Observable<Trade> {
    return this.http.delete<Trade>("http://localhost:59100/api/" + "Trades" + "/" + id);
  }

  getById(id: number): Observable<Trade> {
    return this.http.get<Trade>("http://localhost:59100/api/" + "Trades" + "/" + id);
  }
  getAll(): Observable<Trade[]> {
    return this.http.get<Trade[]>("http://localhost:59100/api/" + "Trades");
  }

  setById(id: number) {
    this.getById(id).subscribe((data) => {
      this.Trade = data;
    },
      (error) => {
        this.Trade = {
          id: null,
          name: '',
        }
      })
  }

  setAll() {
    this.getAll().subscribe((data) => {
      this.Trades = data;
      this.listChanged.next(this.Trades.slice())
    },
      (error) => {
        this.Trades = [];
      })
  }
}
