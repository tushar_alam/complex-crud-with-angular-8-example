import { LanguagesComponent } from './languages/languages.component';
import { LevelsComponent } from './levels/levels.component';
import { TradesComponent } from './trades/trades.component';
import { CrudPageListComponent } from './crudPages/crud-page-list/crud-page-list.component';
import { CrudPageCreateComponent } from './crudPages/crud-page-create/crud-page-create.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path:'crudPageCrate',component:CrudPageCreateComponent},
  {path:'crudPageUpdate/:id',component:CrudPageCreateComponent},
  {path:'crudPageList',component:CrudPageListComponent},

  {path:'trades',component:TradesComponent},
  {path:'levels',component:LevelsComponent},

  {path:'languages',component:LanguagesComponent},
  {path:'', redirectTo:'crudPageList',pathMatch:'full'},
  {path:'**', component:CrudPageListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
