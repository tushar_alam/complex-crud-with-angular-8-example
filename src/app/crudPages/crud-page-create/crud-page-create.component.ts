import { TestModelDetail } from './../shared/test-model-detail';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TestModel } from '../shared/test-model';
import { Trade } from 'src/app/trades/shared/trade';
import { Level } from 'src/app/levels/shared/level';
import { TestModelService } from '../shared/test-model.service';
import { LevelService } from 'src/app/levels/shared/level.service';
import { TradeService } from 'src/app/trades/shared/trade.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { ReadFile } from 'src/app/shared/read-file';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { LanguageService } from 'src/app/languages/shared/language.service';
import { Language } from 'src/app/languages/shared/language';

@Component({
  selector: 'app-crud-page-create',
  templateUrl: './crud-page-create.component.html',
  styleUrls: ['./crud-page-create.component.css']
})
export class CrudPageCreateComponent implements OnInit {

  @ViewChild('form', null) form: NgForm;

  tradeListChangedSubscription: Subscription;
  levelListChangedSubscription: Subscription;
  languagesChangedSubscription: Subscription;

  editMode = false;
  editedTestModelId: number;
  editedTestModel: TestModel;
  tradeList: Trade[] = [];
  levelList: Level[] = [];
  languageList: Language[] = [];

  testModelDetailList: TestModelDetail[] = [];


  InitialTradeId = null;
  InitialLevelId = null;
  activeDate = null;
  datePickerConfig: Partial<BsDatepickerConfig>;

  constructor(private testModelService: TestModelService,
    private tradeService: TradeService,
    private levelService: LevelService,
    private languageService: LanguageService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _readFile: ReadFile) {

    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        showWeekNumbers: true,
        dateInputFormat: 'DD/MM/YYYY'
      });
  }

  ngOnInit() {
    this.tradeService.setAll();
    this.levelService.setAll();
    this.languageService.setAll();
    this.tradeListChangedSubscription = this.tradeService.listChanged.subscribe((data) => {
      this.tradeList = data
    }, (error) => {
      this.tradeList = [];
    });
    this.levelListChangedSubscription = this.levelService.listChanged.subscribe((data) => {
      this.levelList = data
    }, (error) => {
      this.levelList = [];
    });

    this.languagesChangedSubscription = this.languageService.listChanged.subscribe((data) => {
      this.languageList = data
    }, (error) => {
      this.languageList = [];
    });

    this.reset();
    this.setDataByParams();

  }

  onSubmit(form: NgForm) {

    this.editedTestModel = {
      id: this.editMode ? +this.editedTestModelId : 0,
      syllabusName: form.value.syllabusName,
      developmentOfficer: form.value.developmentOfficer,
      manager: form.value.manager,
      activeDate: form.value.activeDate,
      syllabusFile: this.SyllabusResponse != null ? this.SyllabusResponse.dbPath : null,
      testPlaneFile: this.TestPlaneResponse != null ? this.TestPlaneResponse.dbPath : null,
      tradeId: +form.value.tradeId,
      levelId: +form.value.levelId,
      trade: null,
      level: null,
      testModelDetail: this.getTestModelDetailList(this.languageList),
    }

    if (this.editMode) {
      this.testModelService.update(this.editedTestModelId, this.editedTestModel).subscribe((data) => {
        this.toastr.success('Update Successfull', 'Success');
        this.router.navigate(['/crudPageList']);
      },
        (error) => {
          this.toastr.error('Update Faild', 'Error');
        })
    } else {
      this.testModelService.add(this.editedTestModel).subscribe((data) => {
        this.toastr.success('Save Successfull', 'Success');
        this.router.navigate(['/crudPageList']);
      },
        (error) => {
          this.toastr.error('Save Faild', 'Error');
        })
    }
    this.reset(form);
  }

  reset(form?: NgForm) {
    if (form != null && form != undefined) {
      form.reset();
    } else {
      this.form.reset();
    }
    this.editMode = false;
  }

  setDataByParams() {
    let id = this.activatedRoute.snapshot.params['id'];
    if (id != null) {
      this.testModelService.getById(id).subscribe(data => {
        this.editMode = true;
        this.editedTestModelId = id;
        this.editedTestModel = data;
        this.form.setValue({
          syllabusName: this.editedTestModel.syllabusName,
          developmentOfficer: this.editedTestModel.developmentOfficer,
          manager: this.editedTestModel.manager,
          activeDate: this.editedTestModel.activeDate,

          tradeId: this.editedTestModel.tradeId,
          levelId: this.editedTestModel.levelId,

        })
      });
    }
  }

  getTestModelDetailList(languageList:Language[]): TestModelDetail[] {
    var detailList=[];

    languageList.filter(x=>x.isChecked).forEach(lan => {
       var detail:TestModelDetail={
        id:0,
        testModelId:0,
        testModel:null,
        languageId:lan.id,
        language: null
       };

       detailList.push(detail);
     });
   
    return detailList;
  }


  ngOnDestroy() {
    this.tradeListChangedSubscription.unsubscribe();
    this.levelListChangedSubscription.unsubscribe();
    this.languagesChangedSubscription.unsubscribe();
  }


  getLevelByTradeId(id: number) {
    this.levelService.setAllByTradeId(id);
  }

  // syllabusFile: any = null;
  // syllabusFileChanged(e) {
  //   this.syllabusFile = e[0];
  // }


  // testPlanfile: any = null;
  // testPlaneFileChanged(e) {
  //   this.testPlanfile = e[0];
  // }

  // syllabusFileChanged(event) {
  //   this.syllabusFile = event.target.files[0];
  // }
  // testPlaneFileChanged(event) {
  //   this.testPlanfile = event.target.files[0];
  // }

  public SyllabusResponse: { dbPath: '' };
  public syllabusUploadFinished = (event) => {
    this.SyllabusResponse = event;
  }

  public TestPlaneResponse: { dbPath: '' };
  public testPlaneUploadFinished = (event) => {
    this.TestPlaneResponse = event;
  }
}
