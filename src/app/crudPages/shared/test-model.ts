import { Trade } from 'src/app/trades/shared/trade';
import { Level } from 'src/app/levels/shared/level';
import { TestModelDetail } from './test-model-detail';

export class TestModel {
    id:number;
    syllabusName:string;
    developmentOfficer:string;
    manager:string;
    activeDate:Date
    syllabusFile:string;
    testPlaneFile:string;
    tradeId:number;
    levelId:number;
    trade:Trade;
    level:Level;
    testModelDetail:TestModelDetail[]
}
