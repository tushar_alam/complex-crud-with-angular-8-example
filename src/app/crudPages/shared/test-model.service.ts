import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { TestModel } from './test-model';
import { HttpClient } from '@angular/common/http';
import { TestModelDetail } from './test-model-detail';
import { DataTable } from 'src/app/shared/data-table';
import { SearchVm } from 'src/app/shared/search-vm';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class TestModelService {

  startedEditing = new Subject<number>();
  listChanged = new Subject<TestModel[]>();

  testModels: TestModel[] = [];
  testModel: TestModel;

  p: number;
  total: number;
  dataTable: DataTable<SearchVm, TestModel>;


  constructor(private http: HttpClient) {
    this.p = 1;
    this.total = 0;
  }

  add(entity: TestModel): Observable<TestModel> {
    return this.http.post<TestModel>("http://localhost:59100/api/" + "TestModels", entity, httpOptions);
  }

  update(id: number, entity: TestModel): Observable<any> {
    return this.http.put("http://localhost:59100/api/" + "TestModels" + "/" + id, entity);
  }

  delete(id: number): Observable<TestModel> {
    return this.http.delete<TestModel>("http://localhost:59100/api/" + "TestModels" + "/" + id);
  }

  getById(id: number): Observable<TestModel> {
    return this.http.get<TestModel>("http://localhost:59100/api/" + "TestModels" + "/" + id);
  }
  getAll(): Observable<TestModel[]> {
    return this.http.get<TestModel[]>("http://localhost:59100/api/" + "TestModels");
  }

  setById(id: number) {
    this.getById(id).subscribe((data) => {
      this.testModel = data;
    },
      (error) => {
        this.testModel = {
          id: null,
          syllabusName: '',
          developmentOfficer: '',
          manager: '',
          activeDate: new Date(),
          syllabusFile: null,
          testPlaneFile: null,
          tradeId: null,
          levelId: null,
          trade: null,
          level: null,
          testModelDetail: null,
        }
      })
  }

  setAll() {
    this.getAll().subscribe((data) => {
      this.testModels = data;
      this.listChanged.next(this.testModels.slice())
    },
      (error) => {
        this.testModels = [];
      })
  }


  //<<<----------------server side Pagination & search methods starts---------->>>
  getListByPage(dataTable: DataTable<SearchVm, TestModel>): Observable<DataTable<SearchVm, TestModel>> {
    return this.http.post<DataTable<SearchVm, TestModel>>("http://localhost:59100/api/" + "TestModels" + "/Search", dataTable);
  }

  setListByPage() {
    if (this.total != null) {
      this.getListByPage(this.dataTable).subscribe(
        (list) => {
          this.total = list.count;
          this.p = this.dataTable.currentPage;
          this.testModels = list.dataList;
          this.listChanged.next(this.testModels.slice())
        },
        (error) => {
          this.testModels = [];
        });
    }
    else {
      this.p = this.dataTable.currentPage;
    }
  }
  //<<<---------------server side Pagination & search methods ends---------->>>

}
