import { TestModel } from './test-model';
import { Language } from 'src/app/languages/shared/language';

export class TestModelDetail {
    id:number;
    testModelId:number;
    testModel:TestModel;
    languageId:number;
    language: Language;
}
