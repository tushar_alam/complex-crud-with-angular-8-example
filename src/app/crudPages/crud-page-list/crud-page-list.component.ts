import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TestModel } from '../shared/test-model';
import { Router } from '@angular/router';
import { Trade } from 'src/app/trades/shared/trade';
import { TestModelService } from '../shared/test-model.service';
import { TradeService } from 'src/app/trades/shared/trade.service';
import { ConfirmService } from 'src/app/helper/confirm.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalComponent } from 'src/app/helper/confirm-modal/confirm-modal.component';
import { LevelService } from 'src/app/levels/shared/level.service';
import { Level } from 'src/app/levels/shared/level';

@Component({
  selector: 'app-crud-page-list',
  templateUrl: './crud-page-list.component.html',
  styleUrls: ['./crud-page-list.component.css']
})
export class CrudPageListComponent implements OnInit {

  @ViewChild('searchModelForm', null) form: NgForm;

  listChangedSubscribe: Subscription;
  confirmSubscription: Subscription;

  tradeListSubcribtion: Subscription;
  levelListSubcribtion: Subscription;

  testModelList: TestModel[];
  tradeList: Trade[] = [];
  levelList: Level[] = [];
  modalRef: BsModalRef;
  p: number = 1;

  constructor(public testModelService: TestModelService,
    private tradeService: TradeService,
    private levelService: LevelService,
    private modalService: BsModalService,
    private confirmService: ConfirmService,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
    this.reset();
    this.testModelService.setListByPage();
    this.tradeService.setAll();
    this.levelService.setAll();

    this.tradeListSubcribtion = this.tradeService.listChanged.subscribe((data) => { this.tradeList = data }, (error) => {
      this.tradeList = [];
    });
    this.levelListSubcribtion = this.levelService.listChanged.subscribe((data) => { this.levelList = data }, (error) => {
      this.levelList = [];
    });


    this.listChangedSubscribe = this.testModelService.listChanged.subscribe((data) => { this.testModelList = data }, (error) => {
      this.testModelList = [];
    });

    this.confirmSubscription = this.confirmService.deleted.subscribe((data) => {
      if (data) {
        this.testModelService.setListByPage();
        this.toastr.warning('Deleted Successfully', 'Success');
      }
      else {
        this.toastr.warning("You can't Delete this TestModel", 'Warning');
      }
    });
  }

  onEdit(id: number) {
    this.router.navigate(['/crudPageUpdate', id]);
  }

  onSearchCriteriaSubmit(form: NgForm) {
    if (form.valid) {
      this.testModelService.dataTable.searchModel.tradeId = +form.value.tradeId;
      this.testModelService.dataTable.searchModel.levelId = +form.value.levelId;
      this.testModelService.setListByPage();
    }
    else {
      this.toastr.warning('Please specify a search criteria', 'Warning');
    }
  }


  searchCancel() {
    this.reset();
    this.testModelService.setListByPage();
  }

  //<<<--------Pagination methods starts------->>>>
  changePage(page: number) {
    this.testModelService.dataTable.currentPage = page;
    this.testModelService.setListByPage();
  }

  changelistTotal(event: Event) {
    this.testModelService.dataTable.itemsPerPage = event.target['options']
    [event.target['options'].selectedIndex].value;
    this.testModelService.setListByPage();
  }
  //<<<--------Pagination methods ends------->>>>


  onDelete(Id: number) {
    this.openConfirmModal(Id, 'TestModels');
  }

  openConfirmModal(id: number, url: string) {
    this.modalRef = this.modalService.show(ConfirmModalComponent, {
      initialState: {
        title: 'Confirmation!!',
        data: {
          id: id,
          url: url
        }
      }
    });
  }

  reset(form?: NgForm) {
    if (form != null && form != undefined) {
      form.reset();
    } else {
      this.form.reset();
    }
    this.testModelService.dataTable = {
      id: null,
      userId: null,
      serialNo: null,

      canCreate: null,
      canUpdate: null,
      canView: null,
      canDelete: null,

      sortField: '',
      sortOrder: '',
      filter: '',

      count: 0,
      currentPage: 1,
      itemsPerPage: 10,

      dataList: null,
      searchModel: {
        tradeId: null,
        levelId: null,
      }
    }

  }

  ngOnDestroy() {
    this.confirmSubscription.unsubscribe();
    this.listChangedSubscribe.unsubscribe();
    this.tradeListSubcribtion.unsubscribe();
    this.levelListSubcribtion.unsubscribe();
  }


  public createPath = (serverPath: string) => {
    return `http://localhost:59100/${serverPath}`;
  }

}
