import { Trade } from 'src/app/trades/shared/trade';

export class Level {
    id:number;
    name:string;
    tradeId:number;
    trade:Trade;
}
