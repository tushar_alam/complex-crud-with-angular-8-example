import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Level } from './level';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LevelService {

  startedEditing = new Subject<number>();
  listChanged = new Subject<Level[]>();
  deleted = new Subject<boolean>();

  levels: Level[] = [];
  level: Level;
  constructor(private http: HttpClient) { }

  add(entity: Level): Observable<Level> {
    return this.http.post<Level>("http://localhost:59100/api/" + "Levels", entity);
  }

  update(id: number, entity: Level): Observable<any> {
    entity.id = id;
    return this.http.put("http://localhost:59100/api/" + "Levels/" + id, entity);
  }

  delete(id: number): Observable<Level> {
    return this.http.delete<Level>("http://localhost:59100/api/" + "Levels" + "/" + id);
  }

  getById(id: number): Observable<Level> {
    return this.http.get<Level>("http://localhost:59100/api/" + "Levels" + "/" + id);
  }
  getAll(): Observable<Level[]> {
    return this.http.get<Level[]>("http://localhost:59100/api/" + "Levels");
  }

  getAllByTradeId(id: number): Observable<Level[]> {
    return this.http.get<Level[]>("http://localhost:59100/api/Levels/" + "ByTrade/" + id);
  }

  setAllByTradeId(id:number) {
    this.getAllByTradeId(id).subscribe((data) => {
      this.levels = data;
      this.listChanged.next(this.levels.slice())
    },
      (error) => {
        this.levels = [];
      })
  }

  setById(id: number) {
    this.getById(id).subscribe((data) => {
      this.level = data;
    },
      (error) => {
        this.level = {
          id: null,
          name: '',
          tradeId: null,
          trade: null
        }
      })
  }

  setAll() {
    this.getAll().subscribe((data) => {
      this.levels = data;
      this.listChanged.next(this.levels.slice())
    },
      (error) => {
        this.levels = [];
      })
  }
}
