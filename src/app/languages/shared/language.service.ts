import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Language } from './language';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  
  startedEditing = new Subject<number>();
  listChanged = new Subject<Language[]>();
  deleted = new Subject<boolean>();

  languages: Language[] = [];
  language: Language;
  constructor(private http: HttpClient) { }

  add(entity: Language): Observable<Language> {
    return this.http.post<Language>("http://localhost:59100/api/" + "Languages", entity);
  }

  update(id: number, entity: Language): Observable<any> {
    entity.id = id;
    return this.http.put("http://localhost:59100/api/" + "Languages/" + id, entity);
  }

  delete(id: number): Observable<Language> {
    return this.http.delete<Language>("http://localhost:59100/api/" + "Languages" + "/" + id);
  }

  getById(id: number): Observable<Language> {
    return this.http.get<Language>("http://localhost:59100/api/" + "Languages" + "/" + id);
  }
  getAll(): Observable<Language[]> {
    return this.http.get<Language[]>("http://localhost:59100/api/" + "Languages");
  }

  setById(id: number) {
    this.getById(id).subscribe((data) => {
      this.language = data;
    },
      (error) => {
        this.language = {
          id: null,
          name: '',
          isChecked:false
        }
      })
  }

  setAll() {
    this.getAll().subscribe((data) => {
      this.languages = data;
      this.listChanged.next(this.languages.slice())
    },
      (error) => {
        this.languages = [];
      })
  }
}
