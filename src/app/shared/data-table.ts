export class DataTable<TSearchVm, TEntityModel> {
    id :number;
    userId: number;
    serialNo: number;
  
    canCreate: boolean;
    canUpdate: boolean;
    canView: boolean;
    canDelete: boolean;
  
    sortField :string;
    sortOrder :string;
    filter :string;
  
    count: number;
    currentPage: number;
    itemsPerPage: number;
  
    dataList: TEntityModel[];
    searchModel: TSearchVm;
  
  }
  