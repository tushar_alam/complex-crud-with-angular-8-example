import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

import { ConfirmService } from '../confirm.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.css']
})
export class ConfirmModalComponent implements OnInit {

  title;
  data: any;

  constructor(public modalRef: BsModalRef,
    private http: HttpClient,
    private confirmService: ConfirmService
  ) { }

  ngOnInit() {
  }

  onDelete() {
    this.confirmService.delete(this.data.id, this.data.url).subscribe((data) => {
      this.confirmService.deleted.next(true);
      this.modalRef.hide();
    }, (error) => {
      this.confirmService.deleted.next(false);
    });
  }
}
