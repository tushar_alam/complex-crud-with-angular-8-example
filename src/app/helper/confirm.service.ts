import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ConfirmService {

    deleted = new Subject<boolean>();
    constructor(private http: HttpClient) { }

    delete(id: number, url: string): Observable<any> {
        return this.http.delete("http://localhost:59100/api/" + url + "/" + id);
    }
}
