import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import {NgxPaginationModule} from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AlifeFileToBase64Module } from 'alife-file-to-base64';
import { AppRoutingModule } from './app-routing.module';


import { AppComponent } from './app.component';
import { TradesComponent } from './trades/trades.component';
import { TradeComponent } from './trades/trade/trade.component';
import { TradeListComponent } from './trades/trade-list/trade-list.component';
import { LevelsComponent } from './levels/levels.component';
import { LevelComponent } from './levels/level/level.component';
import { LevelListComponent } from './levels/level-list/level-list.component';
import { LanguagesComponent } from './languages/languages.component';
import { LanguageComponent } from './languages/language/language.component';
import { LanguageListComponent } from './languages/language-list/language-list.component';
import { CrudPageCreateComponent } from './crudPages/crud-page-create/crud-page-create.component';
import { CrudPageListComponent } from './crudPages/crud-page-list/crud-page-list.component';
import { ConfirmModalComponent } from './helper/confirm-modal/confirm-modal.component';
import { UploadComponent } from './upload/upload.component';


const customSelectOptions: INgxSelectOptions = { // Check the interface for more options
  optionValueField: 'id',
  optionTextField: 'name',
  keepSelectedItems:true
};


@NgModule({
  declarations: [
    AppComponent,
    TradesComponent,
    TradeComponent,
    TradeListComponent,
    LevelsComponent,
    LevelComponent,
    LevelListComponent,
    LanguagesComponent,
    LanguageComponent,
    LanguageListComponent,
    CrudPageCreateComponent,
    CrudPageListComponent,
    ConfirmModalComponent,
    UploadComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    NgxSelectModule.forRoot(customSelectOptions),
    NgxPaginationModule,
    ModalModule.forRoot(),
    AlifeFileToBase64Module,
    BsDatepickerModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ 
    ConfirmModalComponent
   ]

})
export class AppModule { }
